#!/usr/bin/env python

import codecs
import os
import re
from distutils.core import setup

from setuptools import find_packages

base_path = os.path.abspath(os.path.dirname(__file__))


SEMVER_REGEX = r"(0|[1-9]\d*)\.(0|[1-9]\d*)\.(0|[1-9]\d*)" \
               r"(?:-((?:0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*)" \
               r"(?:\.(?:0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*))*))?" \
               r"(?:\+([0-9a-zA-Z-]+(?:\.[0-9a-zA-Z-]+)*))?"


def read(*parts):
    # intentionally *not* adding an encoding option to open, See:
    #   https://github.com/pypa/virtualenv/issues/201#issuecomment-3145690
    return codecs.open(os.path.join(base_path, *parts), "r").read()


def find_version(*path_parts, version_pattern=SEMVER_REGEX):
    version_data = read(*path_parts)
    pattern = r"^__version__ = [\'\"]{0}[\'\"]$".format(version_pattern)
    version_match = re.search(pattern, version_data, re.M)
    if version_match:
        return version_match.group(1)
    else:
        raise RuntimeError(
            "Unable to find version string. (tried pattern: <{0}>)".format(pattern)
        )


setup(
    name="Celestia",
    version=find_version("celestia", "__init__.py"),
    description="Django extenders",
    author="Gin Fuyou",
    author_email="devel@doratoa.net",
    url="",
    zip_safe=False,
    install_requires=[
        "colorama",
    ],
    packages=find_packages(exclude=("tests*",)),
)
