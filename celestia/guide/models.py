# -*- coding: utf-8 -*-
from django.apps import apps
from django.conf import settings
from django.db import models
from django.template import Context, Template
from django.utils.translation import gettext_lazy as _


def default_priority():
    model_class = apps.get_model(app_label="guide", model_name="GuideClause")
    current_max = model_class.objects.filter(is_active=True).aggregate(
        max_priority=models.Max("priority")
    )["max_priority"]
    if current_max:
        return current_max + 20
    else:
        return 200


class GuideClauseQuerySet(models.QuerySet):
    def public(self):
        return self.filter(is_active=True).select_related()


class ClauseTranslationQuerySet(models.QuerySet):
    def public_for_language(self, language):
        return self.filter(language=language, clause__is_active=True).select_related(
            "clause"
        )


class GuideClause(models.Model):
    class Meta:
        db_table = "celestia_guide_clauses"
        verbose_name = _("clause")
        verbose_name_plural = _("clauses")

    slug = models.SlugField(max_length=128)
    allow_html = models.BooleanField(default=False)

    create_date = models.DateField(auto_now_add=True)

    is_active = models.BooleanField(default=True)

    priority = models.PositiveSmallIntegerField(default=default_priority)

    objects = GuideClauseQuerySet.as_manager()

    def __str__(self):
        return self.slug

    def fetch_images(self, ordering=("priority",)):
        """ load qs of image reference objects. Order is mandatory """
        return self.image_refs.all().select_related("container").order_by(*ordering)


class ClauseTranslation(models.Model):
    class Meta:
        db_table = "celestia_guide_translations"
        unique_together = (("language", "index_title"), ("language", "clause"))

    clause = models.ForeignKey("GuideClause", on_delete=models.CASCADE)
    language = models.CharField(
        max_length=16,
        choices=settings.LANGUAGES,
        default=getattr(settings, "CELESTIA_DEFAULT_LANGUAGE", "en"),
        db_index=True,
    )

    category = models.CharField(max_length=128, blank=True)
    index_title = models.CharField(max_length=256)
    verbose_title = models.CharField(max_length=512, blank=True)
    body = models.TextField(max_length=16384)
    body.help_text = _("use image_refs context variable to load linked Images")
    html = models.TextField(max_length=16384, blank=True)

    update_dt = models.DateTimeField(auto_now=True)

    objects = ClauseTranslationQuerySet.as_manager()

    def save(self, *args, **kwargs):
        if self.clause.allow_html:
            template = Template(self.body)
            con = Context({"image_refs": self.clause.fetch_images()})
            self.html = template.render(con)
            self.full_clean()
        super().save(*args, **kwargs)

    def __str__(self):
        return self.index_title


class GuideImageUsage(models.Model):
    class Meta:
        db_table = "celestia_guide_images"
        unique_together = (
            "container",
            "priority",
        )  # TODO remove this change to (('clause','container'), ('clause', 'priority'))

    container = models.ForeignKey(
        settings.CELESTIA_IMAGE_MODEL, on_delete=models.CASCADE
    )
    clause = models.ForeignKey(
        "GuideClause", on_delete=models.CASCADE, related_name="image_refs"
    )

    priority = models.PositiveSmallIntegerField(default=200)
