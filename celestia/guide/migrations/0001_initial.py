# Generated by Django 2.0.8 on 2018-11-15 08:37

import django.db.models.deletion
from django.db import migrations, models

import celestia.guide.models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ("kern", "0015_auto_20181115_1137"),
    ]

    operations = [
        migrations.CreateModel(
            name="ClauseTranslation",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                (
                    "language",
                    models.CharField(
                        choices=[
                            ("af", "Afrikaans"),
                            ("ar", "Arabic"),
                            ("ast", "Asturian"),
                            ("az", "Azerbaijani"),
                            ("bg", "Bulgarian"),
                            ("be", "Belarusian"),
                            ("bn", "Bengali"),
                            ("br", "Breton"),
                            ("bs", "Bosnian"),
                            ("ca", "Catalan"),
                            ("cs", "Czech"),
                            ("cy", "Welsh"),
                            ("da", "Danish"),
                            ("de", "German"),
                            ("dsb", "Lower Sorbian"),
                            ("el", "Greek"),
                            ("en", "English"),
                            ("en-au", "Australian English"),
                            ("en-gb", "British English"),
                            ("eo", "Esperanto"),
                            ("es", "Spanish"),
                            ("es-ar", "Argentinian Spanish"),
                            ("es-co", "Colombian Spanish"),
                            ("es-mx", "Mexican Spanish"),
                            ("es-ni", "Nicaraguan Spanish"),
                            ("es-ve", "Venezuelan Spanish"),
                            ("et", "Estonian"),
                            ("eu", "Basque"),
                            ("fa", "Persian"),
                            ("fi", "Finnish"),
                            ("fr", "French"),
                            ("fy", "Frisian"),
                            ("ga", "Irish"),
                            ("gd", "Scottish Gaelic"),
                            ("gl", "Galician"),
                            ("he", "Hebrew"),
                            ("hi", "Hindi"),
                            ("hr", "Croatian"),
                            ("hsb", "Upper Sorbian"),
                            ("hu", "Hungarian"),
                            ("ia", "Interlingua"),
                            ("id", "Indonesian"),
                            ("io", "Ido"),
                            ("is", "Icelandic"),
                            ("it", "Italian"),
                            ("ja", "Japanese"),
                            ("ka", "Georgian"),
                            ("kab", "Kabyle"),
                            ("kk", "Kazakh"),
                            ("km", "Khmer"),
                            ("kn", "Kannada"),
                            ("ko", "Korean"),
                            ("lb", "Luxembourgish"),
                            ("lt", "Lithuanian"),
                            ("lv", "Latvian"),
                            ("mk", "Macedonian"),
                            ("ml", "Malayalam"),
                            ("mn", "Mongolian"),
                            ("mr", "Marathi"),
                            ("my", "Burmese"),
                            ("nb", "Norwegian Bokmål"),
                            ("ne", "Nepali"),
                            ("nl", "Dutch"),
                            ("nn", "Norwegian Nynorsk"),
                            ("os", "Ossetic"),
                            ("pa", "Punjabi"),
                            ("pl", "Polish"),
                            ("pt", "Portuguese"),
                            ("pt-br", "Brazilian Portuguese"),
                            ("ro", "Romanian"),
                            ("ru", "Russian"),
                            ("sk", "Slovak"),
                            ("sl", "Slovenian"),
                            ("sq", "Albanian"),
                            ("sr", "Serbian"),
                            ("sr-latn", "Serbian Latin"),
                            ("sv", "Swedish"),
                            ("sw", "Swahili"),
                            ("ta", "Tamil"),
                            ("te", "Telugu"),
                            ("th", "Thai"),
                            ("tr", "Turkish"),
                            ("tt", "Tatar"),
                            ("udm", "Udmurt"),
                            ("uk", "Ukrainian"),
                            ("ur", "Urdu"),
                            ("vi", "Vietnamese"),
                            ("zh-hans", "Simplified Chinese"),
                            ("zh-hant", "Traditional Chinese"),
                        ],
                        db_index=True,
                        default="en",
                        max_length=16,
                    ),
                ),
                ("category", models.CharField(blank=True, max_length=128)),
                ("index_title", models.CharField(max_length=256)),
                ("verbose_title", models.CharField(blank=True, max_length=512)),
                ("body", models.TextField(max_length=16384)),
                ("html", models.TextField(blank=True, max_length=16384)),
                ("update_dt", models.DateTimeField(auto_now=True)),
            ],
            options={
                "db_table": "celestia_guide_translations",
            },
        ),
        migrations.CreateModel(
            name="GuideClause",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("slug", models.SlugField(max_length=128)),
                ("allow_html", models.BooleanField(default=False)),
                ("create_date", models.DateField(auto_now_add=True)),
                ("is_active", models.BooleanField(default=True)),
                (
                    "priority",
                    models.PositiveSmallIntegerField(
                        default=celestia.guide.models.default_priority
                    ),
                ),
            ],
            options={
                "db_table": "celestia_guide_clauses",
                "verbose_name": "clause",
                "verbose_name_plural": "clauses",
            },
        ),
        migrations.CreateModel(
            name="GuideImageUsage",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("priority", models.PositiveSmallIntegerField(default=200)),
                (
                    "clause",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        to="guide.GuideClause",
                    ),
                ),
                (
                    "container",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        to="kern.GalleryImage",
                    ),
                ),
            ],
            options={
                "db_table": "celestia_guide_images",
            },
        ),
        migrations.AddField(
            model_name="clausetranslation",
            name="clause",
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.CASCADE, to="guide.GuideClause"
            ),
        ),
        migrations.AlterUniqueTogether(
            name="guideimageusage",
            unique_together={("container", "priority")},
        ),
        migrations.AlterUniqueTogether(
            name="clausetranslation",
            unique_together={("language", "index_title"), ("language", "clause")},
        ),
    ]
