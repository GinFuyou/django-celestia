from django.apps import AppConfig


class GuideConfig(AppConfig):
    name = "celestia.guide"
