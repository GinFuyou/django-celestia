from django.utils.translation import get_language_from_request
from django.views.generic.list import ListView

from celestia.view_container import ViewContainer

from .models import ClauseTranslation  # , GuideClause


class GuideView(ListView):
    model = ClauseTranslation
    pattern_name = "guide"
    template_name = "celestia_guide/guide.html"
    context_object_name = "clause_translations"
    ordering = ("clause__priority",)

    def get_queryset(self):
        lang = get_language_from_request(self.request)
        print(lang)
        queryset = self.model.objects.public_for_language(language=lang)
        ordering = self.get_ordering()
        if ordering:
            if isinstance(ordering, str):
                ordering = (ordering,)
            queryset = queryset.order_by(*ordering)
        return queryset


views = ViewContainer()
views.register_class(GuideView)
