# -*- coding: utf-8 -*-
"""Celestia.guide URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
"""

from django.urls import path

from .views import views

urlpatterns = [
    path("", views.guide, name=views.guide.pattern_name),
]
