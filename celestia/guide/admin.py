from django import forms
from django.contrib import admin

from . import models

try:
    from django_summernote.widgets import SummernoteWidget as WYSIWYGWidget
except ImportError:
    WYSIWYGWidget = None


class ClauseTranslationForm(forms.ModelForm):
    class Meta:
        model = models.ClauseTranslation
        widgets = {
            "body": WYSIWYGWidget() if WYSIWYGWidget else forms.Textarea(),
        }
        fields = "__all__"


class TranslationInlineForm(forms.ModelForm):
    class Meta:
        model = models.ClauseTranslation
        fields = ("language", "category", "index_title", "verbose_title", "body")
        widgets = {
            "body": WYSIWYGWidget() if WYSIWYGWidget else forms.Textarea(),
        }


class TranslationInline(admin.StackedInline):
    model = models.ClauseTranslation
    extra = 1
    form = TranslationInlineForm
    fieldsets = (
        (
            "",
            {
                "fields": (
                    ("index_title", "language"),
                    ("verbose_title", "category"),
                    "body",
                )
            },
        ),
    )
    can_delete = False


class GuideImageInline(admin.TabularInline):
    model = models.GuideImageUsage
    extra = 2
    fields = ("container", "priority")
    autocomplete_fields = ("container",)


@admin.register(models.GuideClause)
class GuideClauseAdo(admin.ModelAdmin):
    # form = GuideClauseForm
    list_display = ("slug", "id", "is_active", "priority")
    list_filter = ("is_active",)
    inlines = [TranslationInline, GuideImageInline]


@admin.register(models.ClauseTranslation)
class ClauseTranslationAdo(admin.ModelAdmin):
    form = ClauseTranslationForm
    list_display = ["index_title", "language"]
    search_fields = [
        "index_title",
        "title",
        "body",
    ]


@admin.register(models.GuideImageUsage)
class GuideImageUsageAdo(admin.ModelAdmin):
    list_display = ("clause", "container", "priority")
    autocomplete_fields = ("container",)
