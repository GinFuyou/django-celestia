from django.apps import AppConfig


class CelestiaConfig(AppConfig):
    name = 'celestia'
