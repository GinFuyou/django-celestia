# -*- coding: utf-8 -*-

from django.forms import CharField, ChoiceField, Form, HiddenInput


class SessionActionForm(Form):
    def __init__(self, actions, *args, **kwargs):
        self._action_choices = actions
        super().__init__(*args, **kwargs)
        self.fields['action'].choices = actions

    action = ChoiceField(widget=HiddenInput(), choices=())
    value = CharField(widget=HiddenInput(), required=False)
