from celery import shared_task
from celery.utils.log import get_task_logger
from django.apps import apps
from django.conf import settings
from django.core.mail import send_mail

# from django.utils.timezone import localtime

logger = get_task_logger(__name__)


@shared_task(bind=True, rate_limit="12/m", expires=3600 * 48, max_retries=2)
def delayed_mail(self, subj, msg, mail_to, model_to_update=None):
    if not mail_to:
        mail_to = [x[1] for x in settings.ADMINS]
    logger.info("Mail to: {0}".format(', '.join(mail_to)))
    try:
        mail_to = list(set(mail_to))
        result = send_mail(subj, msg, settings.EMAIL_HOST_USER, mail_to,
                           fail_silently=False, html_message=msg)
    except Exception as exc:
        logger.error("Exception sending mail: {0}".format(exc))
        raise self.retry(exc=exc)
    if result:
        logger.debug('Mail sent: {0}'.format(result))
        if model_to_update:
            logger.debug('model_to_update dict: {0!r}'.format(model_to_update))
            model_class = apps.get_model(app_label=model_to_update['app'],
                                         model_name=model_to_update['model'])
            instance = model_class.objects.get(pk=model_to_update['pk'])
            setattr(instance, model_to_update['attr'], True)
            instance.save()
        return "Mailed to {0}".format(', '.join(mail_to))
    else:
        raise self.retry()
    return False
