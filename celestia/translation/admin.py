from django.conf import settings


class TransFormSetMixin:
    _extra = len(settings.LANGUAGES)
    # formset = TransInlineFormSet
    translations_related_name = "translations"

    def get_extra(self, request, obj=None, **kwargs):
        extra = self._extra
        if obj:
            existing_count = getattr(obj, self.translations_related_name).count()
            extra = max(0, extra - existing_count)
        # print(f"get_extra -> {extra}")
        return extra
