# -*- coding: utf-8 -*-

from collections import namedtuple

from django.conf import settings
from django.db import models
from django.utils.translation import get_language, get_language_from_request

# from django.utils.translation import gettext_lazy as _


def safe_key(key):
    return key.replace('-', '_').upper()


LangChoiceClass = namedtuple('lang_code_namedtuple', [safe_key(i[0]) for i in settings.LANGUAGES])
LANG_CHOICES = LangChoiceClass(*settings.LANGUAGES)


class BaseTranslatedQuerySet(models.QuerySet):
    base_object_field = "base"

    def translated_by_request(self, request, check_path=True):
        lang_code = get_language_from_request(request, check_path=check_path)
        # print(f"language code: {lang_code}")
        return self.translated(lang_code)

    def translated(self, lang_code=None):
        if not lang_code:
            lang_code = get_language()
        return self.filter(lang_code=lang_code).select_related(self.base_object_field)

    def for_language(self, lang_code):
        """ depricate? """
        return self.filter(lang_code=lang_code).select_related(self.base_object_field)


class AbstractTranslatedModel(models.Model):
    class Meta:
        abstract = True

        constraints = [
            models.UniqueConstraint(
                fields=('base', 'lang_code'),
                name='%(app_label)s_%(class)s_unique_for_lang')
        ]

    lang_code = models.SlugField(
        max_length=16,
        db_index=True,
        choices=LANG_CHOICES,
        default=getattr(LANG_CHOICES, safe_key(settings.LANGUAGE_CODE), ('en',))[0],
    )
