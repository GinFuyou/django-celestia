# -*- coding: utf-8 -*-
import warnings


class ViewContainer(object):

    def __init__(self):
        self.viewnames = []

    def to_snake_case(self, camel):
        prev_is_lower = False
        output = []
        for c in camel:
            if c.isupper():
                if prev_is_lower:
                    output += ['_', c.lower()]
                else:
                    output.append(c.lower())
            else:
                output.append(c)
            prev_is_lower = c.islower()
            s = "".join(output)
            s = s.replace('_view', '')
        return s

    def register_function(self, view, function_name=None):
        if not function_name:
            function_name = view.__name__
        self.viewnames.append(function_name)
        setattr(self, function_name, view)

    def register_class(self, view_class, function_name=None):
        class_name = view_class.__name__
        if not function_name:
            function_name = self.to_snake_case(class_name)
        view = view_class.as_view()
        if hasattr(view_class, 'pattern_name'):
            view.pattern_name = view_class.pattern_name

        self.register_function(view, function_name)
        # compatibility
        setattr(self, function_name+'_view', view)

    def __add__(self, other):
        if isinstance(other, self.__class__):
            for viewname in other.viewnames:
                if hasattr(self, viewname):
                    msg = "ViewContainer addition overrides '{}'".format(viewnames)
                    warnings.warn(msg, RuntimeWarning)
                else:
                    self.viewnames.append(viewname)
                view = getattr(other, viewname)
                setattr(self, viewname, view)
        else:
            raise TypeError(
                "ViewContainer supports addition only with same or" \
                " descendant class, not '{0}'".format(type(other))  )
        return self


def register_view(container, function_name=None):
    """
    Register the given model(s) classes and wrapped ModelAdmin class with
    admin site:
    @register(Author)
    class AuthorAdmin(admin.ModelAdmin):
        pass
    The `site` kwarg is an admin site to use instead of the default admin site.
    """

    def _view_wrapper(view_object):
        if hasattr(view_object, 'as_view'):
            container.register_class(view_object, function_name)
        else:
            container.register_function(view_object, function_name)

        return view_object
    return _view_wrapper
