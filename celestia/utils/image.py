import logging
from pathlib import Path

from django.conf import settings
from PIL import Image

logger = logging.getLogger("django")


def make_preview(filepath, unlink=True, **kwargs):

    target_size = kwargs.get('size')
    quality = kwargs.get('quality', 92)
    prefix = kwargs.get('prefix', 'preview ')
    final_path_function = kwargs.get(
        'final_path_function',
        lambda filepath, **kwargs: filepath.parent / "prev/"
    )

    blank_if_nonexistent = kwargs.get("blank_if_nonexistent", True)

    filepath = Path(filepath)

    if not filepath.is_file() and blank_if_nonexistent:
        return ""

    im = Image.open(filepath)

    tmp_path = kwargs.get('tmp_path', filepath.parent)

    if kwargs.get('pil_verify', True):
        im.verify()
        im = Image.open(filepath)

    stem = filepath.stem
    im.thumbnail(target_size)

    preview_path = tmp_path / f"{prefix}{stem}_q{quality}.webp"
    im.save(preview_path, "WEBP", quality=quality, method=kwargs.get('method', 5))

    final_path = final_path_function(filepath, **kwargs)
    final_path.mkdir(mode=0o775, parents=True, exist_ok=True)

    preview_path = preview_path.rename(final_path / preview_path.name)
    rel_path = preview_path.relative_to(settings.MEDIA_ROOT)
    logger.debug(F"Made preview at '{preview_path}'")
    return rel_path
