import logging
import re
from collections import namedtuple

from django.conf import settings

logger = logging.getLogger("django")

# CELESTIA_ALLOWED_NESTED_EXTS expects *NOT* dotted extensions, e.g. ['tar', 'gz', 'jpeg']
_ALLOWED_EXTS = getattr(settings, 'CELESTIA_ALLOWED_NESTED_EXTS', None)
MultiExtNamePartsTuple = namedtuple('MultiExtNameParts', ('pure_stem', 'subexts', 'ext'))


def split_multiple_ext(path, allowed_extensions=_ALLOWED_EXTS, to_lower=True):
    """ split pathlib.Path into basename, ext and list of added extensions
        e.g. spam.tar.gz -> ('spam', ['.tar'], '.gz')
        NOTE returns pure_stem which is cleared of sequential spaces and dots
        Target of this function is not following standards but common practice
    """

    stem = path.stem
    ext = path.suffix
    if to_lower:
        ext = ext.lower()
        stem = stem.lower()

    # replace sequential spaces with single " "
    stem = re.sub(r"\s{2,}", " ", stem)

    if '.' in stem:
        logger.debug(f"split_multiple_ext: dot in stem '{stem}', processing...")
        # replace sequential dots with single
        stem = re.sub(r"\.+", ".", stem)

        # save for later flag if filename starts with dot
        dot_prefix = (stem[0] == '.')
        if dot_prefix:
            stem = stem[1:]

        splits = stem.split('.')
        pure_stem = splits[0]
        # return prefixed dot now, when split by dot was done
        if dot_prefix:
            pure_stem = "." + pure_stem
        subexts = splits[1:]

        if allowed_extensions:
            test_lambda = lambda i: i in allowed_extensions
        else:
            test_lambda = lambda i: i not in ('', ' ')

        subexts = [f".{i}" for i in subexts if test_lambda(i)]
    else:
        pure_stem = stem
        subexts = []
    logger.debug(f"split_multiple_ext: {pure_stem} | {subexts!r} | {ext}")
    return MultiExtNamePartsTuple(pure_stem, subexts, ext)
