# -*- coding: utf-8 -*-

from django.contrib import messages
from django.shortcuts import redirect
from django.views.generic.edit import FormView

from celestia.forms import SessionActionForm


class BaseSessionUpdateView(FormView):
    form_class = SessionActionForm
    success_url = '/'
    template_name = None
    pattern_name = 'session_update'
    action_messages = {}
    action_perms = {}
    action_choices = ()

    def get_success_url(self):
        url = self.request.GET.get('next')
        if url and url.startswith('/'):
            return url
        else:
            return self.success_url

    def get_form_kwargs(self):
        """Return the keyword arguments for instantiating the form."""
        kwargs = {'initial': self.get_initial(),
                  'prefix': self.get_prefix(),
                  'actions': self.action_choices}
        if self.request.method in ('POST', 'PUT'):
            kwargs.update({'data': self.request.POST})
        return kwargs

    def form_valid(self, form):
        action = form.cleaned_data['action']
        value = form.cleaned_data.get('value', False)
        flag = False
        if action in self.action_perms:
            permission = self.action_perms[action]
            if type(permission) is str:
                flag = self.request.user.has_perm(permission)
            elif permission is True:
                flag = True
        if flag is True:
            self.request.session[action] = value
            if action in self.action_messages:
                messages.add_message(self.request, messages.SUCCESS, self.action_messages[action])
        else:
            messages.add_message(self.request, messages.ERROR, "Command permission denied!")
        return redirect(self.get_success_url())

    def form_invalid(self, form):
        messages.add_message(self.request, messages.ERROR, "Unknown command")
        return redirect(self.get_success_url())
