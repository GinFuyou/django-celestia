# from django.conf import settings
from django.core.exceptions import ValidationError
from django.utils.translation import ungettext


class MinMaxLengthValidator(object):
    """
    Validate whether the password is of a minimum length.
    """
    def __init__(self, min_length=8, max_length=24):
        self.min_length = min_length
        self.max_length = max_length

    def validate(self, password, user=None):
        if len(password) < self.min_length:
            raise ValidationError(
                ungettext(
                    "This password is too short. It must contain at least %(min_length)d"
                    " character.",
                    "This password is too short. It must contain at least %(min_length)d"
                    " characters.",
                    self.min_length
                ),
                code='password_too_short',
                params={'min_length': self.min_length},
            )
        if self.max_length and len(password) > self.max_length:
            raise ValidationError(
                ungettext(
                    "This password is too long. It must contain at max %(max_length)d character.",
                    self.max_length
                ),
                code='password_too_long',
                params={'max_length': self.max_length},
            )

    def get_help_text(self):
        return ungettext(
            "Your password must contain at least %(min_length)d character.",
            "Your password must contain at least %(min_length)d characters.",
            self.min_length
        ) % {'min_length': self.min_length}
