# -*- coding: utf-8 -*-

from collections import namedtuple

from django.utils.translation import activate, get_language

CurrencyNamedTuple = namedtuple(
    "currency_tuple", ("iso4217_code", "iso4217_num", "verbose_name", "usymbol")
)


# a helper function of making namedtuples
def choices_fab(names, tuplename="choices_namedtuple"):
    if type(names) == str:
        names = names.split(" ")
    if type(names) == tuple or type(names) == list:
        TupleClass = namedtuple(tuplename, names)
        values = []
        counter = 0
        for name in names:
            name = name.capitalize().replace(" ", "_").replace("'", "")
            values.append((counter, name.capitalize()))
            counter += 1

        instance = TupleClass._make(values)
        # print (instance) # TEST
        return instance
    else:
        return None


def choices_fab2(names, tuplename="choices_namedtuple"):
    if type(names) == str:
        names = names.split(" ")
    if type(names) == tuple or type(names) == list:
        counter = 0
        values = []
        safe_names = []
        for name in names:
            name = name.replace(" ", "_").replace("'", "")
            values.append((counter, name))
            safe_names.append(name)
            counter += 1
        TupleClass = namedtuple(tuplename, safe_names)

        instance = TupleClass._make(values)
        return instance
    else:
        return None


# a helper function of making namedtuples
def choices_trans_fab(names, tuplename="translable_choices_namedtuple"):
    if type(names) == tuple or type(names) == list:
        lang = get_language()
        activate("en")
        values = []
        counter = 0
        for name in names:
            # name_copy = deepcopy(name)
            str(name).lower().replace(" ", "_").replace("'", "")
            # print(safe_name, ':', )
            values.append((counter, name))
            counter += 1
        TupleClass = namedtuple(tuplename, name)
        activate(lang)

        instance = TupleClass._make(values)
        return instance
    else:
        return None


""" choices_fab usage example

    _tsukecat_seq = ('common',
                     'artist',
                     'character',
                     'origin',
                     'album',
                     'auto',
                     'create')

    TSUKECAT_CHST = choices_fab(_tsukecat_seq, 'tg_category_namedtuple')
    category = models.PositiveSmallIntegerField(
        choices = TSUKECAT_CHST, default = TSUKECAT_CHST.common[0])
"""


_universal_phone_class = namedtuple(
    "universal_phone_description", ("regex", "hint", "hint_ru", "examples")
)
UNIVERSAL_PHONE = _universal_phone_class(
    regex=r"^\+\d{1,3}\s\d{1,5}\s\d[\d\-]{3,9}\d$",
    hint="+X<country code> XXX<city or operator code> XXX-XX-XX<number>",
    hint_ru="+X<код страны> XXX<код города или оператора> XXX-XX-XX<номер>",
    examples=("+7 495 000-00-00", "+44 20 0000-0000", "+7 473 000-0000"),
)
