from django.conf import settings
from django.template.loader import get_template

from celestia.tasks import delayed_mail


class CrumbsMixin():
    """ Mixin for adding cbreadcrumbs """
    def get_crumbs(self):
        if self.crumbs:
            return self.crumbs
        else:
            return None

    def get_context_data(self, **kwargs):
        context = {}
        crumbs = self.get_crumbs()
        if crumbs:
            context['crumbs'] = crumbs
        context.update(kwargs)
        return super().get_context_data(**context)


class MailerMixin():
    """ add method to send mails via delayed task """
    mail_template_name = None

    def send_mail(self, subj, context, mail_to=settings.ADMINS):
        try:
            template = get_template(self.mail_template_name)
            msg = template.render(context)
        except Exception as exc:
            error_msg = "Rendering Mail Error: {0}".format(exc)
            print(error_msg)
            delayed_mail.delay(
                "[{0}] Rendering Mail Error".format(getattr(settings, 'PROJECT_NAME', 'Django')),
                error_msg,
                mail_to=settings.ADMINS
            )
        else:
            delayed_mail.delay(subj, msg, mail_to=mail_to)
