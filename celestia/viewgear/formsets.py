import logging

from django.forms import modelformset_factory
from django.shortcuts import redirect
from django.urls import reverse

logger = logging.getLogger("django")


class ModelFormsetMixin:
    form_fields = []
    formset_extra = 3
    formset_model = None
    form_class = None
    commit_forms = False
    ordering = None
    formset_kwargs = None
    success_url = None
    save_on_change = True
    initial = {}

    def get_success_url(self):
        if self.success_url:
            return self.success_url
        else:
            return reverse(self.pattern_name)

    def post(self, request, *args, **kwargs):
        """
        Handle POST requests: instantiate a form instance with the passed
        POST variables and then check if it's valid.
        """
        self.parent_object = self.get_object()
        self.object = self.parent_object
        formset = self.get_formset(post_data=request.POST)
        if formset.is_valid():
            return self.formset_valid(formset)
        else:
            return self.formset_invalid(formset)

    def get_formset_queryset(self, **kwargs):
        qs = self.formset_model._default_manager.filter(
            **self.get_formset_filters(**kwargs)
        )
        if self.ordering:
            qs = qs.order_by(*self.ordering)
        self.formset_queryset = qs
        return qs

    def get_formset_filters(self, **kwargs):
        return dict()

    def get_initial(self):
        return self.initial.copy()

    def get_formset(self, post_data=None):
        kwargs = {
            "queryset": self.get_formset_queryset(),
            "initial": self.get_initial(),
        }
        if post_data:
            kwargs["data"] = post_data
        return self.get_formset_class(**self.formset_kwargs)(**kwargs)

    def formset_valid(self, formset):
        # print("formset is valid")
        for form in formset:
            # obj = self.process_form(form)
            self.process_form(form)
        return redirect(self.get_success_url())

    def process_form(self, form):
        """if emptiness field is set, test form data to have a value in this field
        check supposed to fail explicitly.
        """
        if self.save_on_change:
            if form.has_changed():
                obj = form.save(commit=self.commit_forms)
            else:
                obj = None
        else:
            obj = form.save(commit=self.commit_forms)
        return obj

    def formset_invalid(self, formset, error_anchor=None):
        if error_anchor is True:
            for form in formset:
                if form.is_valid() is False:
                    error_anchor = form.prefix
                    break
        return self.render_to_response(
            self.get_context_data(formset=formset, error_anchor=error_anchor)
        )

    def get_formset_class(self, **kwargs):
        return modelformset_factory(
            model=self.formset_model,
            fields=self.form_fields,
            extra=self.get_extra_count(),
            **kwargs
        )

    def get_extra_count(self):
        # Implement dynamic extra if needed
        return self.formset_extra

    def get_context_data(self, **kwargs):
        if "formset" not in kwargs:
            kwargs["formset"] = self.get_formset()
        con = super().get_context_data(**kwargs)
        con["formset_queryset"] = self.formset_queryset
        return con
