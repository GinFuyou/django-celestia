# -*- coding: utf-8 -*-

from django.conf import settings
from django.core.exceptions import ImproperlyConfigured
from django.utils.translation import gettext_lazy as _


def setting(request):
    names = getattr(settings, 'SETTING_CONTEXT_NAMES', ('PROJECT_VERSION',))
    d = {}
    for constname in names:
        if 'SECRET' in constname: 
            msg = _("Context processor 'setting': adding key '%(key)s' is insecure")
            raise ImproperlyConfigured(msg, 
                                       params={'key': constname},
                                       code='potentially_insecure')
        d[constname] = getattr(settings, constname, '')
    return d
