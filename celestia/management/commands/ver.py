# -*- coding: utf-8 -*-

import os
from collections import OrderedDict, namedtuple
from datetime import date

import yaml
from django.conf import settings
from django.core.management.base import BaseCommand

# === ver ============================= | v |
__rumversion__ = 2.2
__prerelease__ = "a4"
__version__ = "{rumver:.2f}{prerelease}".format(
    rumver=__rumversion__, prerelease=__prerelease__
)
# ===================================== | v |


class Command(BaseCommand):
    help = "Update project or app version and write changelog. (ver.{0})".format(
        __version__
    )

    def add_arguments(self, parser):
        parser.add_argument("appname", type=str, help="name of project or app")
        parser.add_argument(
            "up",
            type=int,
            help="integer count of minor versions to up",
            default=0,
            nargs="?",
        )
        parser.add_argument(
            "release", type=str, help="pre-release suffix", default="", nargs="?"
        )
        parser.add_argument(
            "-s",
            "--show",
            dest="show",
            action="store_true",
            help="show version and quit",
        )

    VersionTuple = namedtuple(
        "version_tuple",
        ["rumversion", "rumversion_old", "prerelease", "prerelease_old"],
    )

    def touch_init(self, initpath, up_rv=0.0, up_pr=0, allow_write=False):
        filelines = []
        up_rv = round(up_rv, 2)
        with open(initpath, "r") as f:
            filelines = []
            for line in f:
                if line.startswith("__rumversion__"):
                    rv = float(line.split("=")[1].strip())
                    rv_prev = rv
                    if up_rv != 0.0:
                        rv += up_rv
                        rv = round(rv, 2)
                        line = "__rumversion__ = {0:.2f}\n".format(rv)
                elif line.startswith("__prerelease__"):
                    pr = line.split("=")[1].strip().strip('"').strip("'")
                    pr_prev = pr
                    if up_pr != 0:
                        pr = up_pr
                        if pr == "r":
                            pr = ""
                        line = "__prerelease__ = '{0}'\n".format(pr)
                filelines.append(line)

        if allow_write and (up_rv != 0.0 or up_pr != 0):
            with open(initpath, "w") as f:
                for line in filelines:
                    f.write(line)

        return self.VersionTuple(rv, rv_prev, pr, pr_prev)

    # ===================================================== | m |
    def update_changelog(self, appname, rv, pr="", fname="changelog.yaml"):
        fullpath = os.path.join(settings.BASE_DIR, appname, fname)

        rep = self.stdout.write

        chlog_ordict = OrderedDict()
        chlog_ordict[rv] = ["1st entry", "2nd entry", "3rd"]

        # request changelog input ===== #
        msg_list = []
        msg = ""
        rep(
            "Input changlog messages for {0} {1} ('done' when finished)".format(
                appname, rv
            )
        )
        while msg not in ("done", "q", "stop"):
            msg = input()
            if msg not in ("done", "q", "stop") and len(msg) > 3:
                msg_list.append(msg)

        # parse and update changelog == #
        if os.path.isfile(fullpath):
            with open(fullpath, "r") as f:
                chlog_ordict = yaml.load(f)
            # append message to list if same numeric version or create new dict item
            if rv in chlog_ordict.keys():
                chlog_ordict[rv] += msg_list
            else:
                chlog_ordict[rv] = msg_list
            # insert into list current date if not present
            date_str = str(date.today())
            if chlog_ordict[rv][0] != date_str:
                chlog_ordict[rv].insert(0, date_str)

        # write changelog ============= #
        with open(fullpath, "w") as f:
            yaml.dump(chlog_ordict, f, default_flow_style=False)

        for key, value in chlog_ordict.items():
            rep("{0} : {1}".format(key, value))

        return fullpath

    # ===================================================== | h |
    def handle(self, *args, **options):
        rep = self.stdout.write
        rep("Celestia ver command v.{0} kidou!".format(__version__))

        appname = options["appname"]
        ver_up = options["up"]
        release = options["release"]

        # rep(repr(options))
        # appname aquired - check it

        path = os.path.join(settings.BASE_DIR, "{0}".format(appname))

        if os.path.exists(path):
            initpath = os.path.join(path, "__init__.py")

            if options["show"]:
                vnt = self.touch_init(initpath)
                rep("{1} {0.rumversion_old}{0.prerelease_old}".format(vnt, appname))
                return

            if ver_up <= 0 and release == "":
                vnt = self.touch_init(initpath)
            elif release == "":
                rumversion_up = round(int(ver_up) / 100, 2)
                vnt = self.touch_init(initpath, rumversion_up, allow_write=True)
            else:
                rumversion_up = round(int(ver_up) / 100, 2)
                vnt = self.touch_init(
                    initpath, rumversion_up, release, allow_write=True
                )

            rep(appname)
            rep(
                "{0.rumversion_old} -> {0.rumversion}\n"
                " {0.prerelease_old} -> {0.prerelease}".format(
                    vnt
                )
            )
            self.update_changelog(appname, vnt.rumversion, vnt.prerelease)
        else:
            rep("path '{0}' is not confirmed!".format(path))
