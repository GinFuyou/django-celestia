# -*- coding: utf-8 -*-

import os
import re
from subprocess import STDOUT, Popen
from sys import executable
from time import sleep

from django.conf import settings

from celestia.management.base_extend import BasePonyCommand  # , CommandError

try:
    from jsmin import jsmin

    JSMIN = True
except ImportError:
    JSMIN = False


class Command(BasePonyCommand):
    name = "SASS"
    version = "1.0a2"
    help = "Shortcut command to manipulate sass build and deployment. (ver.{0})".format(
        version
    )

    def add_arguments(self, parser):
        parser.add_argument(
            "--style",
            action="store",
            type=str,
            choices=("nested", "compact", "compressed", "expanded"),
            default="compressed",
            help="set sass output style (default changed to compressed)"
                 " ['nested','compact', 'compressed', 'expanded']",
        )
        parser.add_argument(
            "-j",
            "--js",
            action="store_true",
            help="combine and minify js in <APP>/src/js/",
        )
        parser.add_argument("-m", "--map", action="store_true", help="use sass map")
        parser.add_argument(
            "-M", "--nomin", action="store_true", help="combine js without minifying"
        )
        # parser.add_argument('-p','--push', dest='push', action='store',
        # nargs="?", default=False, const="+",
        # help='commit and push. promt if no arg supplied')

    def runprocess(self, arglist, cmd="sass", pollstep=0.35, shell=False):
        arglist.insert(0, cmd)
        returncode = None
        process = Popen(arglist, stderr=STDOUT, stdout=None, shell=shell)
        while returncode is None:
            sleep(pollstep)
            returncode = process.poll()
        return returncode

    def _app_combine_loop(self, p, app):
        ls = sorted(os.listdir(p))
        combines = []
        cmb_mtime = 0.0
        for fname in ls:
            src_path = os.path.join(p, fname)
            if os.path.isfile(src_path):
                name, ext = os.path.splitext(fname)
                if ext == ".js":
                    src_mtime = os.path.getmtime(src_path)
                    if name.startswith("cmb"):
                        combines.append((src_path, name))
                        cmb_mtime = max(src_mtime, cmb_mtime)
                    else:
                        trg_path = os.path.join(
                            settings.BASE_DIR, app, "static", "js", name + ".min" + ext
                        )
                        if os.path.isfile(trg_path):
                            if src_mtime <= os.path.getmtime(trg_path):
                                continue
                        else:
                            os.makedirs(
                                os.path.join(settings.BASE_DIR, app, "static", "js"),
                                exist_ok=True,
                            )
                        self.vprint(
                            "* minimify {0}{1} -> {2}".format(name, ext, trg_path), v=1
                        )
                        f = open(src_path)
                        m = jsmin(f.read())
                        f.close()
                        f = open(trg_path, "w")
                        f.write(m)
                        f.close()

                else:
                    continue  # file is not .js
            else:
                continue  # not a file
        # END loop
        return combines

    def combine_js(self):
        if not JSMIN:
            self.vprint("JS: jsmin not installed, skipping!", "RED", v=1)
            return
        if not hasattr(settings, "CELESTIA_STATIC_APPS"):
            self.vprint("JS: CELESTIA_STATIC_APPS setting not set!", "RED", v=1)
            return

        for app in settings.CELESTIA_STATIC_APPS:
            p = os.path.join(settings.BASE_DIR, app, "src", "js")
            if os.path.isdir(p):
                combines = self._app_combine_loop(p, app)
                if len(combines) > 0:
                    combine_path = os.path.join(
                        settings.BASE_DIR,
                        app,
                        "static",
                        "js",
                        "{0}.combo.js".format(app),
                    )
                    data = ""
                    names = []
                    for path, name in combines:
                        names.append(name)
                        f = open(path)
                        if self.args.nomin:
                            data += "\n/* {0} */\n".format(name) + f.read()
                        else:
                            data += jsmin(f.read()) + "\n"
                        f.close()
                    f = open(combine_path, "w")
                    f.write(data)
                    f.close()
                    self.vprint(
                        "* combine {0} -> {1}".format(", ".join(names), combine_path),
                        v=1,
                    )
            else:
                continue

    def handle(self, *args, **options):

        main_style_filename = getattr(settings, "MAIN_STYLE_FILENAME", "main")
        var_path = os.path.join(
            settings.BASE_DIR, settings.STYLES_APP, "sass", "_pythonvars.sass"
        )
        self.vprint("var_path: {0}".format(var_path))
        with open(var_path, "r") as f:
            data = f.read()
            build = 1
            m = re.search(r"^\$sass_build\:\s(?P<build>\d+)$", data, flags=re.MULTILINE)
            if m:
                build = int(m.group("build"))
                build += 1
            self.vprint("SASS Build: {0}".format(build), v=1)
        with open(var_path, "w") as f:
            data = '$static_url: "{0}"\n$sass_build: {1}'.format(
                settings.STATIC_URL, build
            )
            if self.args.verbosity > 1:
                self.cprint("Writing variables for SASS: {0}".format(data))
            f.write(data)

        extra_sass_args = []
        if self.args.map:
            pass
        else:
            extra_sass_args.append("--sourcemap=none")

        sass_path = os.path.join(
            settings.BASE_DIR,
            settings.STYLES_APP,
            "sass",
            main_style_filename + ".sass",
        )
        self.vprint("sass_path: {0}".format(sass_path))
        css_path = os.path.join(
            settings.BASE_DIR,
            settings.STYLES_APP,
            "static",
            main_style_filename + ".css",
        )
        sass_arg = "{0}:{1}".format(sass_path, css_path)
        self.runprocess(
            [
                sass_arg,
                "--style",
                self.args.style,
            ] + extra_sass_args
        )

        if hasattr(settings, "SASS_LIST"):
            self.vprint("Extra separate SASS files:", v=1)
            for app, name in settings.SASS_LIST:
                sass_path = os.path.join(settings.BASE_DIR, app, "sass", name + ".sass")
                css_path = os.path.join(settings.BASE_DIR, app, "static", name + ".css")
                self.vprint(
                    "  {0} -> {1}".format(
                        sass_path,
                        css_path,
                    ),
                    v=1,
                    colour="YELLOW",
                )
                sass_arg = "{0}:{1}".format(sass_path, css_path)
                self.runprocess(
                    [sass_arg, "--style", self.args.style] + extra_sass_args
                )
            self.vprint(" ", v=1)

        if self.args.js:
            self.combine_js()

        self.vprint(
            'NOTICE: will call collectstatic with "{0}" executable'.format(executable)
        )
        self.runprocess(["manage.py", "collectstatic", "--noinput"], cmd=executable)
