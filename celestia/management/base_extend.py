# -*- coding: utf-8 -*-

import sys
import warnings

from colorama import Fore, Style
from django.core.exceptions import ImproperlyConfigured
from django.core.management.base import (BaseCommand, CommandError,
                                         SystemCheckError,
                                         handle_default_options)
from django.db import connections
from django.utils.timezone import localtime
from django.utils.timezone import now as django_now

# from argparse import


class DummyStyle(object):
    def __getattr__(self, attr):
        return ""


class ColoramaFormatter(object):
    RESET_ALL = Style.RESET_ALL
    colours = ["RED", "GREEN", "YELLOW", "BLUE", "MAGENTA", "CYAN", "WHITE"]

    def __getitem__(self, key):
        if key == "RESET_ALL":
            return self.reset_all
        elif type(key) == int:
            key = key % len(self.colours)
            fore = self.colours[key]
        else:
            fore = getattr(Fore, key.upper(), "WHITE")
        return fore

    @property
    def reset_all(self):
        return self.RESET_ALL


class BasePonyCommand(BaseCommand):
    help = ""
    args = ""
    name = "BasePonyCommand"
    version = "0.0"
    start_text = "* {1.YELLOW}{0.name}{2.RESET_ALL} ver.{1.MAGENTA}{0.version}{2.RESET_ALL}" \
                 " kidou! (v={0.args.verbosity})"
    fmt = ColoramaFormatter()

    # Configuration shortcuts that alter various logic.
    _called_from_command_line = False
    can_import_settings = True
    output_transaction = False  # Whether to wrap the output in a "BEGIN; COMMIT;"
    leave_locale_alone = False

    def cprint(self, text, clrz=None, end="\n", pre_time=False):
        warnings.warn(
            "!! cprint() is deprecated in favour to vprint()", DeprecationWarning
        )
        if clrz:
            try:
                text = clrz(text)
            except Exception as exc:
                self.stdout.write("Couldn't colourize: {0}".format(exc))
        if pre_time:
            text = "{0} {1}".format(localtime(django_now()).strftime("%H:%M:%S"), text)
        self.stdout.write(text, ending=end)

    def vprint(self, text, v=2, end="\n", colour=None):
        if self.args.verbosity >= v:
            if colour:
                f = getattr(Fore, colour.upper(), "WHITE")
                text = "{0}{1}{2.RESET_ALL}".format(f, text, Style)
            self.stdout.write(text, ending=end)

    def run_from_argv(self, argv):
        """
        Set up any environment changes requested (e.g., Python path
        and Django settings), then run this command. If the
        command raises a ``CommandError``, intercept it and print it sensibly
        to stderr. If the ``--traceback`` option is present or the raised
        ``Exception`` is not ``CommandError``, raise it.
        """
        self._called_from_command_line = True
        parser = self.create_parser(argv[0], argv[1])

        options = parser.parse_args(argv[2:])
        self.args = options
        cmd_options = vars(options)
        # Move positional args out of options to mimic legacy optparse
        args = cmd_options.pop("args", ())
        handle_default_options(options)
        try:
            self.execute(*args, **cmd_options)
        except Exception as e:
            if options.traceback or not isinstance(e, CommandError):
                raise

            # SystemCheckError takes care of its own formatting.
            if isinstance(e, SystemCheckError):
                self.stderr.write(str(e), lambda x: x)
            else:
                self.stderr.write("%s: %s" % (e.__class__.__name__, e))
            sys.exit(1)
        finally:
            try:
                connections.close_all()
            except ImproperlyConfigured:
                # Ignore if connections aren't setup at this point (e.g. no
                # configured settings).
                pass

    def handle(self, *cargs, **options):
        raise NotImplementedError(
            "subclasses of BasePonyCommand must provide a handle() method"
        )
