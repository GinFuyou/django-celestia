# -*- coding: utf-8 -*-
import re


class PhoneNumber(object):
    regex = re.compile(r"^(\+\d{1,5}\s)?(\d{1,5})\s(\d{4,9})$")
    default_format = "c (a) xxx-xx-xx"

    def __init__(self, number_string, default_format=None):
        """accepts numbers like +7 900 123456789 or 900 123456789"""
        if not self.regex.match(number_string):
            raise ValueError(
                "PhoneNumber argument must be a string like '+7 900 123456789' or '900 123456789'"
            )

        splits = number_string.split(" ")
        if len(splits) == 3:
            self.country, self.area, self.number = splits
        elif len(splits) == 2:
            self.area, self.number = splits
            self.country = ""

        self.local_len = len(self.number)

        if default_format:
            self.default_format = default_format.lower()
        self.validate_format(self.default_format)

        self.human = self.spec_format(self.default_format)

    def validate_format(self, fmt):
        if fmt.count("x") != self.local_len:
            raise ValueError(
                "PhoneNumber format number of 'x' must match len of local number part"
            )
            return False
        else:
            return True

    def spec_format(self, fmt):
        count = fmt.count("x")
        self.validate_format(fmt)
        s = fmt
        for i in range(0, count):
            s = s.replace("x", self.number[i], 1)
        s = s.replace("c", self.country).replace("a", self.area)
        return s

    def __str__(self):
        return "".join((self.country, self.area, self.number))

    def format(self, fmt=None):
        if fmt is None:
            return self.human

    def add_format(self, name, fmt):
        if self.validate_format(fmt):
            s = self.spec_format(fmt)
            setattr(self, name, s)
            return True
        else:
            return False

    def url(self):
        return "tel://" + str(self)

    def link(self):
        return "<a href='{0}'>{1}</a>".format(self.url(), self.human)
