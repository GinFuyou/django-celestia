import logging
import os
import warnings
from pathlib import Path
from subprocess import PIPE, run
from time import time

from django.core.exceptions import ImproperlyConfigured
from django.db import models
from django.utils import timezone
from django.utils.translation import gettext_lazy as _

logger = logging.getLogger("django")


class AbstractDateTimeTrackedModel(models.Model):
    """Very basic abstract model that adds
    update_datetime and create_datetime
    """

    class Meta:
        abstract = True

    update_datetime = models.DateTimeField(
        verbose_name=_("date and time updated"), auto_now=True
    )
    create_datetime = models.DateTimeField(
        verbose_name=_("date and time created"), default=timezone.now
    )


class FileProcessingMixin:
    """Model Mixin to post-process FileFields like generating hashes
    returns a list of derived fields method going to update
    add _file_fields_to_process attribute to your model like this:
        filefield_name:              # name of FileField or it's subclass
            derived_field_name:      # name of field derived from that FileField (e.g. hashsum)
                function: callable   # processing function, e.g. hashlib.sha1
                args: []             # list of args for the function
                kwargs: {}           # dict of kwargs for the function
                attr: None           # if function returns an object, get it's attr or method
                preprocessor: None   # function or keyword to get data from file
                postprocessor: None  # callable to post-process the result
                update_datetime_field: None


    example:
    _file_fields_to_process = {
        'file': {
            'checksum': {'function': hashlib.sha1, 'attr': 'hexdigest'}
            'crc32': {'function': zlib.crc32, 'postprocessor': lambda i: f"{i:x}"}
        }
    }
    """

    #
    _file_fields_to_process = {}
    _file_fields_to_cleanup = []

    def _process_file_with_sys_bin(self, exec_path, file_path, args=[]):
        exec_path = Path(exec_path).resolve()
        if not os.access(exec_path, os.X_OK):
            msg = f"{exec_path} is not a file, executable or access is restricted!"
            logger.error(msg)
            raise ImproperlyConfigured(msg)

        file_path = Path(file_path).resolve()
        args = [exec_path, file_path] + args
        result = run(args, encoding="utf-8", stdout=PIPE, stderr=PIPE, timeout=15)
        if result.stderr:
            logger.error(f"Error handling sys binary: '{args}' - {result.stderr}")
        if result.stdout:
            return result.stdout.strip()
        else:
            return None

    def process_files(self, fields, raise_error=False, run_cleaning=True):
        derived_fields_to_update = []
        for fieldname in fields:
            # skip fields not listed for this model
            derived_fields = self._file_fields_to_process.get(fieldname, None)
            if derived_fields is None:
                msg = (
                    f"Improper usage of FileProcessingMixin: Field '{fieldname}'"
                    f" not in dict self._file_fields_to_process"
                )
                if raise_error:
                    raise RuntimeError(msg)
                else:
                    logger.error(msg)
                    warnings.warn(msg)
                    continue

            logger.info(
                f"Post-processing file field '{fieldname}': {repr(derived_fields)}"
            )
            for derived_field_name, handler in derived_fields.items():
                function = getattr(handler, "function", None)
                args = getattr(handler, "args", [])
                kwargs = getattr(handler, "kwargs", {})
                attr = getattr(handler, "attr", None)
                preprocessor = getattr(
                    handler, "preprocessor", "__bytes__"
                )  # get file bytes
                postprocessor = getattr(handler, "postprocessor", None)
                update_datetime_field = getattr(handler, "update_datetime_field", None)
                timer = -time()
                if callable(handler):
                    function = handler
                elif isinstance(handler, dict):
                    function = handler["function"]
                    args = handler.get("args", [])
                    kwargs = handler.get("kwargs", {})
                    attr = handler.get("attr", None)
                    preprocessor = handler.get(
                        "preprocessor", "__bytes__"
                    )  # get file bytes
                    postprocessor = handler.get("postprocessor", None)
                    update_datetime_field = handler.get("update_datetime_field")
                if not function:
                    raise NotImplementedError(
                        f'Handler "{handler!r}" method is of not implemented'
                        f' for field "{fieldname}" -> "{derived_field_name}"'
                    )
                # check if function is python callable or reference to external subroutine
                old_value = getattr(self, derived_field_name)
                value = None
                file_field = getattr(self, fieldname)

                if callable(function):
                    if preprocessor == "__bytes__":
                        with file_field.open() as f:
                            data = f.read()
                    elif preprocessor == "__field__":
                        data = file_field
                    else:
                        data = preprocessor(file_field)
                    value = function(data, *args, **kwargs)
                    # if function returns result which is not directly usable,
                    # get attr from it (e.g. 'hexdigest') and call it if it's a callable

                elif isinstance(function, str) and function.startswith("/"):
                    # handle function as system binary
                    value = self._process_file_with_sys_bin(function, file_field.path)

                else:
                    raise NotImplementedError(
                        f"Non-callable function <{function!r}> is not implemented"
                        f' for field "{fieldname}" -> "{derived_field_name}"'
                    )

                if not value:
                    raise RuntimeError(
                        f"Handler <{function}> didn't prpduce any sensible value ({value!r})"
                    )
                # post-process value we got, if needed:
                if attr:
                    value = getattr(value, attr)
                    if callable(value):
                        value = value()
                if postprocessor:
                    value = postprocessor(value)

                # finally add derived_field_name to derived_fields_to_update for save()
                if value != old_value:
                    setattr(self, derived_field_name, value)
                    if update_datetime_field:
                        setattr(self, update_datetime_field, timezone.now())
                        derived_fields_to_update.append(update_datetime_field)
                    derived_fields_to_update.append(derived_field_name)
                timer += time()
                logger.info(f"Processing {derived_field_name}: {timer:.4f} sec.")
        # exit both loops

        # optinally - validate instance (default) '
        if derived_fields_to_update:
            logger.info(
                f"Derived fields to update: {', '.join(derived_fields_to_update)}"
            )
            if run_cleaning:
                self.full_clean()
            self.save(
                update_fields=derived_fields_to_update,
                no_file_cleanup=True,
                postprocess_files=False,
            )
        else:
            logger.info("No derived fields changed")

        return derived_fields_to_update

    def save(self, *args, postprocess_files=True, no_file_cleanup=False, **kwargs):
        cleanup_fields = {}
        if not no_file_cleanup and postprocess_files and self.pk:
            logger.info(f"Fetch old instance for file cleanup pk={self.pk}")
            old_instance = self._meta.model.objects.get(pk=self.pk)
            cleanup_fields = {
                fld: getattr(old_instance, fld)
                for fld in self._file_fields_to_cleanup
            }
        logger.debug("call super.save()")
        super().save(*args, **kwargs)

        logger.warning(
            f"FileProcessingMixin.save() postprocess_files={postprocess_files}"
        )

        if postprocess_files:
            self.process_files(self._file_fields_to_process.keys())

        if not no_file_cleanup:
            for field, old_path in cleanup_fields.items():

                # old_path is a FileField, if it was blank, you can't get .path of it
                if old_path:
                    old_path = old_path.path
                else:
                    continue

                new_path = getattr(self, field).path
                offset = len(field) + 2
                logger.info(f'{field}: {old_path}\n{"-": <{offset}}{new_path}')
                if old_path != new_path:
                    path = Path(old_path).resolve()
                    try:
                        path.unlink()
                    except OSError as exc:
                        msg = f"Failure: couldn't cleanup <{old_path}>: {exc}"
                        logger.error(msg)
                    else:
                        msg = f"Cleanup removal of <{old_path}> successful"
                        logger.warning(msg)
