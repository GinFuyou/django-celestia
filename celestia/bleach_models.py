# using this module requires `bleach` python library to be installed!
import bleach
from django.utils.safestring import mark_safe

# exclude `a`, `script`, `style`, other still better be checked for safety
BLEACH_ALLOW_SVG = [
    "altGlyph",
    "altGlyphDef",
    "altGlyphItem",
    "animate",
    "animateColor",
    "animateMotion",
    "animateTransform",
    "circle",
    "clipPath",
    "color-profile",
    "cursor",
    "defs",
    "desc",
    "ellipse",
    "feBlend",
    "feColorMatrix",
    "feComponentTransfer",
    "feComposite",
    "feConvolveMatrix",
    "feDiffuseLighting",
    "feDisplacementMap",
    "feDistantLight",
    "feFlood",
    "feFuncA",
    "feFuncB",
    "feFuncG",
    "feFuncR",
    "feGaussianBlur",
    "feImage",
    "feMerge",
    "feMergeNode",
    "feMorphology",
    "feOffset",
    "fePointLight",
    "feSpecularLighting",
    "feSpotLight",
    "feTile",
    "feTurbulence",
    "filter",
    "font",
    "font-face",
    "font-face-format",
    "font-face-name",
    "font-face-src",
    "font-face-uri",
    "foreignObject",
    "g",
    "glyph",
    "glyphRef",
    "hkern",
    "image",
    "line",
    "linearGradient",
    "marker",
    "mask",
    "metadata",
    "missing-glyph",
    "mpath",
    "path",
    "pattern",
    "polygon",
    "polyline",
    "radialGradient",
    "rect",
    "set",
    "stop",
    "svg",
    "switch",
    "symbol",
    "text",
    "textPath",
    "title",
    "tref",
    "tspan",
    "use",
    "view",
    "vkern"
]


class BleachMixin():
    BLEACH_SAVE_FIELDS = None  # must be a dict or falsey value
    BLEACH_SHOW_FIELDS = None  # nust be a dict or falsey value
    
    """
    WIP
    def save(self, *args, **kwargs):
        if self.BLEACH_SAVE_FIELDS
        for fieldname, conf in self.BLEACH_SAVE_FIELDS:
            field = getattr(self, fieldname)
            field
    """
    
    def _bleach_field(self, fieldname):
        """ define own bleach_FIELDNAME and call this method """
        value = getattr(self, fieldname, '')
        if self.BLEACH_SHOW_FIELDS:
            bleach_kwargs = self.BLEACH_SHOW_FIELDS.get(fieldname, {})
        else:
            bleach_kwargs = {}
        return mark_safe(bleach.clean(value, **bleach_kwargs))
