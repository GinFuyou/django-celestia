# -*- coding: utf-8 -*-

import re

from django import template
from django.utils.safestring import mark_safe

register = template.Library()

phone_regex = re.compile("[^0-9+]")


@register.filter
def solidrate_fa5(rating, classes='fa-star'):
    """ draw stars based on int rating """
    emptyclass = "far"
    fillclass = 'fas'
    iconhtml = '<i class="{0} {1}"></i>'

    stars = int(rating)  # 4.3 -> 4

    classes = classes.strip()

    html = ''
    symbol = iconhtml.format(fillclass, classes)
    for i in range(0, stars):
        html += symbol
    if stars < 5:
        symbol = iconhtml.format(emptyclass, classes)
        for i in range(0, 5 - stars):
            html += symbol
    return mark_safe(html)


@register.filter
def phone2link(string):
    # clear symbols from phone number string
    string = phone_regex.sub('', string)
    return string


@register.filter
def space2nbsp(string):
    string = string.replace(" ", "&nbsp;")
    return mark_safe(string)


@register.filter
def cbreadcrumbs(iterable_of_tuples, symbol=' / '):
    # takes an iterable of (url, title)
    t = '<a href="{0[0]}">{0[1]}</a>'
    crumbs = []
    for tpl in iterable_of_tuples:
        crumbs.append(t.format(tpl))
    html = symbol.join(crumbs)
    return mark_safe(html)


class MakeListNode(template.Node):
    def __init__(self, var_name, args, group_by):
        self.args = args
        self.var_name = var_name
        self.group_by = group_by

    def render(self, context):
        cleaned_list = []
        for i in self.args:
            cleaned_list.append(i.strip(i[0]))
        if self.group_by > 1:
            nested_list = []
            while len(cleaned_list) >= self.group_by:
                nested_list.append(cleaned_list[:self.group_by])
                cleaned_list = cleaned_list[self.group_by:]
            context[self.var_name] = nested_list
        else:
            context[self.var_name] = cleaned_list
        # print(context[self.var_name])
        return ''


@register.tag
def make_list(parser, token):
    tag_name = "make_list"
    token_pattern = re.compile(r"make_list ([\"\'].+[\"\']) of (\d+) as (\w+)")
    args_pattern = re.compile(r'(\"(?:[^\"]*)\"|\'(?:[^\']*)\')')

    # This version uses a regular expression to parse tag contents.
    m = token_pattern.match(token.contents)
    if not m:
        raise template.TemplateSyntaxError(
            fr"{tag_name} tag requires arguments: at least 1 list element in quotes, "
            fr"then 'of (\d+) as (\w+)'")

    args = m.group(1)
    group_by = int(m.group(2))
    var_name = m.group(3)

    parsed_args = args_pattern.findall(args)
    if not parsed_args:
        raise template.TemplateSyntaxError(f"{tag_name} tag couldn't parse arguments: '{args}'")

    return MakeListNode(var_name, parsed_args, group_by)


@register.filter
def add_script_attrs(script_html, attrs):
    """ append to script tags specific attrs """
    script_head = '<script '
    attrs = attrs.split(" ")
    if script_html.startswith(script_head):
        for attr in attrs:
            attr = attr.strip()
            if f" {attr} " not in script_html:
                script_html = script_html.replace(script_head, f"{script_head}{attr} ")
    return mark_safe(script_html)
